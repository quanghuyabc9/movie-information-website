
// Function for index.html
async function loadTopRated(api_key) {
    const region = "VN";
    const language = "en-US";
    const response = await fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}&language=${language}&region=${region}`);
    const survey = await response.json();
    let total_pages = survey.total_pages;

    for (let page = 1; page <= total_pages; page++) {
        if (page == 1) loadPage_TopRated(api_key, language, page, region);
        $("#nav-top-rated ul li:last-child").before(`
            <li class="page-item"><a class="page-link" href="#" onclick="loadPage_TopRated('${api_key}', '${language}', ${page}, '${region}')">${page}</a></li>
        `);
    }

    $(document).ready(function () {
        $("#nav-top-rated ul li:first-child a").click(function () {
            let curPage = parseInt($("#nav-top-rated .active a").text());
            let prePage = curPage - 1;
            if (prePage >= 1) {
                loadPage_TopRated(api_key, language, prePage, region);
            }
        });
        $("#nav-top-rated ul li:last-child a").click(function () {
            let curPage = parseInt($("#nav-top-rated .active a").text());
            let nextPage = curPage + 1;
            if (nextPage <= total_pages) {
                loadPage_TopRated(api_key, language, nextPage, region);
            }
        });
    })
}

async function loadPage_TopRated(api_key, language, page, region) {
    const response = await fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}&language=${language}&page=${page}&region=${region}`);
    const movies = await response.json();
    //console.log(movies);
    $("#nav-top-rated .row").empty();
    for (const movie of movies.results) {
        $(`
            <div class="col-sm-2 mb-3">
                <div class="card" style="max-width: 200px;">
                    <img src="https://image.tmdb.org/t/p/w500${movie.poster_path}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h6 class="card-title text-truncate">${movie.title}</h5>
                        <p class="card-subtitle text-muted text-truncate">Vote: ${movie.vote_average}</p>
                    </div>
                    <a href="#" class="stretched-link" onclick="openDetailMovie('${movie.id}')"></a>
                </div>
            </div>
            `).appendTo("#nav-top-rated .row");
    }

    $("#nav-top-rated .active").removeClass("active");
    $(`#nav-top-rated ul li:nth-child(${1 + page})`).addClass("active");
}

function openDetailMovie(movie_id) {
    let url = "./detail_movie.html?movie_id=" + encodeURIComponent(movie_id);
    document.location.href = url, true;
}

function searchButton_onclick() {
    let movie_name = $("form input").val();
    let url = "./search_results.html?name=" + encodeURIComponent(movie_name);
    document.location.href = url, true;

}

// Function for search_results.html
async function loadSearchResults(api_key, query) {
    const language = "en-US", include_adult = "false", region = "VN";
    const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=${api_key}&language=${language}&query=${query}&include_adult=${include_adult}&region=${region}`);
    const survey = await response.json();
    let total_pages = survey.total_pages;
    //let total_pages = 2;
    for (let page = 1; page <= total_pages; page++) {
        if (page == 1) loadPage_SearchResults(api_key, query, language, include_adult, region, page);
        $(".alert + .row + nav ul li:last-child").before(
            `
        <li class="page-item">
            <a class="page-link" href="#" onclick="loadPage_SearchResults('${api_key}', '${query}', '${language}', '${include_adult}', '${region}', ${page})">
                ${page}
            </a>
        </li>
        `)
    }

    $(document).ready(function () {
        $(".alert + .row + nav ul li:first-child a").click(function () {
            let curPage = parseInt($(".alert + .row + nav .active a").text());
            let prePage = curPage - 1;
            if (prePage >= 1) {
                loadPage_SearchResults(api_key, query, language, include_adult, region, prePage);
            }
        });
        $(".alert + .row + nav ul li:last-child a").click(function () {
            let curPage = parseInt($(".alert + .row + nav .active a").text());
            let nextPage = curPage + 1;
            if (nextPage <= total_pages) {
                loadPage_SearchResults(api_key, query, language, include_adult, region, nextPage);
            }
        });
    })
}

async function loadPage_SearchResults(api_key, query, language, include_adult, region, page) {
    const response = await fetch(`https://api.themoviedb.org/3/search/movie?page=${page}&api_key=${api_key}&language=${language}&query=${query}&include_adult=${include_adult}&region=${region}`);
    const movies = await response.json();
    $(".alert + .row").empty();
    for (const movie of movies.results) {
        $(`
        <div class="col-sm-2 mb-3">
            <div class="card" style="max-width: 200px;">
                <img src="https://image.tmdb.org/t/p/w500${movie.poster_path}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h6 class="card-title text-truncate">${movie.title}</h5>
                    <p class="card-subtitle text-muted text-truncate">Vote: ${movie.vote_count}</p>
                </div>
                <a href="#" class="stretched-link" onclick="openDetailMovie('${movie.id}')"></a>
            </div>
        </div>
        `).appendTo(".alert + .row");
    }

    $(".alert + .row + nav .active").removeClass("active");
    $(`.alert + .row + nav ul li:nth-child(${1 + page})`).addClass("active");
}

// Function for detail_movie.html
async function loadDetailMovie(api_key, movie_id) {
    const language = "en-US";
    //get movie detail
    const response_detail = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}?api_key=${api_key}&language=${language}`);
    const movie_detail = await response_detail.json();

    //get movie credit
    const response_credit = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=${api_key}`);
    const movie_credit = await response_credit.json();

    //append poster
    $(".card .row .col-md-4").append(`
        <img src="https://image.tmdb.org/t/p/w500${movie_detail.poster_path}" class="card-img" alt="poster">
    `);

    let genres = "";
    for (const genre of movie_detail.genres) {
        genres += genre.name + ', ';
    }

    let crew = "";
    for (const person of movie_credit.crew) {
        if (person.job.indexOf("Director") > -1) {
            crew += person.name + '(' + person.job + ')' + ", ";
        }
    }
    //append title, date release, overview, genres(thể loại), screw
    $(".card div div:last-child #CastLabel").before(`
        <h1 class="card-title">${movie_detail.title}</h2>
        <p class="card-text"><small class="text-muted">Date release: ${movie_detail.release_date}</small></p>
        <p class="card-text"><div class="font-weight-bold">Overview: </div>${movie_detail.overview}</p>
        <p class="card-text"><strong>Gengres: </strong>${genres}</p>
        <p class="card-text"><strong>Directors:  </strong>${crew}</p>
    `);

    //append actors
    for (const actor of movie_credit.cast)
        $(`
        <a class="nav-link" href="#" onclick="openDetailActor('${actor.id}')">${actor.name}</a>
    `).appendTo(".card div div:last-child div nav");


    //get the user reviews for a movie
    const response_reviews = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=${api_key}&language=${language}`);
    const movie_reviews = await response_reviews.json();
    for (let page = 1; page <= movie_reviews.total_pages; page++) {
        if (page == 1) loadPage_Reviews(movie_id, api_key, language, page);
        $("#ReviewsOfMovie + nav ul li:last-child").before(
            `
        <li class="page-item">
            <a class="page-link" href="#" onclick="loadPage_Reviews('${movie_id}', '${api_key}', '${language}', ${page})">
                ${page}
            </a>
        </li>
        `)
    }
    $(document).ready(function () {
        $("#ReviewsOfMovie + nav ul li:first-child a").click(function () {
            let curPage = parseInt($("#ReviewsOfMovie + nav .active a").text());
            let prePage = curPage - 1;
            if (prePage >= 1) {
                loadPage_Reviews(movie_id, api_key, language, prePage);
            }
        });
        $("#ReviewsOfMovie + nav ul li:last-child a").click(function () {
            let curPage = parseInt($("#ReviewsOfMovie + nav .active a").text());
            let nextPage = curPage + 1;
            if (nextPage <= total_pages) {
                loadPage_Reviews(movie_id, api_key, language, nextPage);
            }
        });
    })
}
async function loadPage_Reviews(movie_id, api_key, language, page) {
    const response = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=${api_key}&language=${language}&page=${page}`);
    const reviews = await response.json();
    $("#ReviewsOfMovie").empty();
    for (const review of reviews.results) {
        $(`
        <div class="row no-gutters" style="max-width: 400px;">
            <div class="col-md-2">
                <img src="./images/user.png" class="card-img" alt="user_img">
            </div>
            <div class="col-md-10 bg-secondary text-light">
                <div class="card-body">
                   ${review.author}
                </div>
            </div>
        </div>
        <div class="card-body">
            <p class="card-text">${review.content}</p>
        </div>
        <br/>
        `).appendTo("#ReviewsOfMovie");
    }

    $("#ReviewsOfMovie + nav .active").removeClass("active");
    $(`#ReviewsOfMovie + nav ul li:nth-child(${1 + page})`).addClass("active");
}

// Function for search_by_name.html
async function loadSearchByName(api_key, query) {
    await $("hr + div + .row").empty();
    await $("hr + div + .row + nav ul").empty();
    await $("hr + div + .row + nav ul").append(`
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        `);

    const language = "en_US";
    const response = await fetch(`https://api.themoviedb.org/3/search/person?api_key=${api_key}&language=${language}&query=${query}`);
    const survey = await response.json();
    const total_pages = survey.total_pages;
    for (let page = 1; page <= total_pages; page++) {
        if (page == 1) loadPage_SearchByName(api_key, query, language, page);
        $("hr + div + .row + nav ul li:last-child").before(
            `
        <li class="page-item">
            <a class="page-link" href="#" onclick="loadPage_SearchByName('${api_key}', '${query}', '${language}', ${page})">
                ${page}
            </a>
        </li>
        `)
    }

    $(document).ready(function () {
        $("hr + div + .row + nav ul li:first-child a").click(function () {
            let curPage = parseInt($("hr + div + .row + nav .active a").text());
            let prePage = curPage - 1;
            if (prePage >= 1) {
                loadPage_SearchByName(api_key, query, language, prePage);
            }
        });
        $("hr + div + .row + nav ul li:last-child a").click(function () {
            let curPage = parseInt($("hr + div + .row + nav .active a").text());
            let nextPage = curPage + 1;
            if (nextPage <= total_pages) {
                loadPage_SearchByName(api_key, query, language, nextPage);
            }
        });
    })
}

async function loadPage_SearchByName(api_key, query, language, page) {
    const response = await fetch(`https://api.themoviedb.org/3/search/person?api_key=${api_key}&language=${language}&query=${query}&page=${page}`);
    const actors = await response.json();
    $("hr + div + .row").empty();
    for (const actor of actors.results) {
        for (const known_for of actor.known_for) {
            $(`
                <div class="col-sm-2 mb-3">
                    <div class="card" style="max-width: 200px;">
                        <img src="https://image.tmdb.org/t/p/w500${known_for.poster_path}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h6 class="card-title text-truncate">${known_for.title}</h5>
                            <p class="card-subtitle text-muted">Vote: ${known_for.vote_average}</p>
                            <br/>
                            <p class="card-subtitle text-muted">Result for ${actor.name}</p>
                        </div>
                        <a href="#" class="stretched-link" onclick="openDetailMovie('${known_for.id}')"></a>
                    </div>
                </div>
            `).appendTo("hr + div + .row");
        }
    }

    $("hr + div + .row + nav .active").removeClass("active");
    $(`hr + div + .row + nav ul li:nth-child(${1 + page})`).addClass("active");
}

function searchByNameBtn_onclick() {
    $("#loader").show();
    setTimeout(showPage, 1000);
    const api_key = "a0ea7510f9880114b8066e6885c8ab31";
    let query = $("form #SearchByNameInput").val();
    loadSearchByName(api_key, query);
}

// Function for detail_actor.html
async function loadDetailActor(api_key, person_id) {
    const language = "en-US";
    //get movie detail
    const response_detail = await fetch(`https://api.themoviedb.org/3/person/${person_id}?api_key=${api_key}&language=${language}`);
    const person_detail = await response_detail.json();

    //get movie credit
    const response_credit = await fetch(`https://api.themoviedb.org/3/person/${person_id}/movie_credits?api_key=${api_key}&language=${language}`);
    const person_credit = await response_credit.json();

    //append poster
    $(".card .row .col-md-4").append(`
        <img src="https://image.tmdb.org/t/p/w500${person_detail.profile_path}" class="card-img" alt="poster">
    `);


    $(".card div div:last-child .card-body").append(`
        <h1 class="card-title">${person_detail.name}</h2>
        <p class="card-text"><div class="font-weight-bold">Biography: </div>${person_detail.biography}</p>
    `);

    for (const cast of person_credit.cast) {
        $("#ListMovieLabel + .row").append(`
        <div class="col-sm-2">
            <div class="card" style="max-width: 200px;">
                <img src="https://image.tmdb.org/t/p/w500${cast.poster_path}" class="card-img-top" alt="poster">
                <div class="card-body">
                    <h5 class="card-title">${cast.title}</h5>
                    <p class="card-text">As character: ${cast.character}</p>
                </div>
                <a href="#" class="stretched-link" onclick="openDetailMovie('${cast.id}')"></a>
            </div>
        </div>
        `)
    }
}

function openDetailActor(person_id) {
    let url = "./detail_actor.html?person_id=" + encodeURIComponent(person_id);
    document.location.href = url, true;
}

function showPage() {
    $("#loader").hide();
}
